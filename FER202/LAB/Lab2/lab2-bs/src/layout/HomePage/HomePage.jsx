import React from "react";
import Header from "../../components/Header";
import HeroSection from "./HeroSection";
import Menu from "./Menu";
import BookTable from "./BookTable";

export default function HomePage() {
  return (
    <div>
      <Header />
      <HeroSection />
      <Menu />
      <BookTable />
    </div>
  );
}
