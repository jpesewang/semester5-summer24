import React from "react";

export default function BookTable() {
  return (
    <div>
      <div class="container row-margin">
        <div class="row  py-5">
          <h3 class="white-style center-text">BOOK YOUR TABLE</h3>
          <div class="col-md-4">
            <input
              type="text"
              class="form-control"
              placeholder="Your Name *"
              aria-label="Username"
              aria-describedby="basic-addon1"
            />
          </div>

          <div class="col-md-4">
            <input
              type="text"
              class="form-control"
              placeholder="Your Email *"
              aria-label="Username"
              aria-describedby="basic-addon1"
            />
          </div>

          <div class="col-md-4">
            <select class="form-select" id="validationCustom04" required>
              <option selected disabled value="">
                Select a service
              </option>
              <option>Book dishes</option>
              <option>Shipping</option>
              <option>...</option>
            </select>
          </div>

          <div class="form-container row-margin ">
            <textarea
              class="form-control text-style"
              id="exampleFormControlTextarea1"
              placeholder="Please write your comment"
              rows="3"
            ></textarea>
          </div>
          <div class="row-margin">
            <button class="btn-orange col-2">SEND MESSAGE</button>
          </div>
        </div>
      </div>
    </div>
  );
}
