import React from "react";

export default function Menu() {
  return (
    <div class="container row-margin">
      <div class="row">
        <h5 class="white-style">OUR MENU</h5>

        {/* Product 1 */}
        <div class="col-md-3">
          <div class="card" style={{ width: "17.687rem" }}>
            <div class="sale-badge">SALE</div>
            <img
              src="https://images.unsplash.com/photo-1613564834361-9436948817d1?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8cGl6emF8ZW58MHwxfDB8fHwy"
              class="card-img-top img-style2"
              alt="..."
            />
            <div class="card-body">
              <div class="container">
                <div class="row">
                  <p class="card-text">Product</p>
                  <p class="card-text col-4">
                    <s>$40.00</s>
                  </p>
                  <p class="card-text col-8 color-style">$24.00</p>
                  <button class="btn btn-block col-12 menu-btn">BUY</button>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Product 2 */}
        <div class="col-md-3">
          <div class="card" style={{ width: "17.687rem" }}>
            <img
              src="https://images.unsplash.com/photo-1594007654729-407eedc4be65?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8cGl6emF8ZW58MHwxfDB8fHwy"
              class="card-img-top img-style2"
              alt="..."
            />
            <div class="card-body">
              <div class="container">
                <div class="row">
                  <p class="card-text">Product</p>
                  <p class="card-text col-6">$25.00</p>
                  <button class="btn btn-red btn-block col-12 menu-btn">
                    BUY
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Product 3 */}
        <div class="col-md-3">
          <div class="card" style={{ width: "17.687rem" }}>
            <div class="sale-badge">NEW</div>
            <img
              src="https://images.unsplash.com/photo-1571066811602-716837d681de?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8cGl6emF8ZW58MHwxfDB8fHwy"
              class="card-img-top img-style2"
              alt="..."
            />
            <div class="card-body">
              <div class="container">
                <div class="row">
                  <p class="card-text">Product</p>
                  <p class="card-text col-6">$30.00</p>

                  <button class="btn btn-red btn-block col-12 menu-btn">
                    BUY
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Product 4 */}
        <div class="col-md-3">
          <div class="card" style={{ width: "17.687rem" }}>
            <div class="sale-badge">SALE</div>
            <img
              src="https://images.unsplash.com/photo-1541745537411-b8046dc6d66c?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8cGl6emF8ZW58MHwxfDB8fHwy"
              class="card-img-top img-style2"
              alt="..."
            />
            <div class="card-body">
              <div class="container">
                <div class="row">
                  <p class="card-text">Product</p>
                  <p class="card-text col-4">
                    <s>$50.00</s>
                  </p>
                  <p class="card-text col-8 color-style">$30.00</p>
                  <button class="btn btn-red btn-block col-12 menu-btn">
                    BUY
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
