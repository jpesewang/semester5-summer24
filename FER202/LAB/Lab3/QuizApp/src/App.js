import "./App.css";
import Quiz from "./layout/Quiz";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
function App() {
  return (
    <div className="App">
      <Quiz />
    </div>
  );
}

export default App;
