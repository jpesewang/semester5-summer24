const quizData = [
  {
    id: 1,
    question: "What is the capital of France?",
    options: ["Paris", "London", "Berlin", "Madrid"],
    answer: "Paris",
  },
  {
    id: 2,
    question: "What is the largest planet in our Solar System?",
    options: ["Earth", "Mars", "Jupiter", "Saturn"],
    answer: "Jupiter",
  },
  {
    id: 3,
    question: "Which element has the chemical symbol 'O'?",
    options: ["Oxygen", "Osmium", "Oganesson", "Oxidium"],
    answer: "Oxygen",
  },
  {
    id: 4,
    question: "In which year did the Titanic sink?",
    options: ["1912", "1905", "1898", "1923"],
    answer: "1912",
  },
  {
    id: 5,
    question: "Who wrote 'Romeo and Juliet'?",
    options: [
      "William Shakespeare",
      "Charles Dickens",
      "Jane Austen",
      "Mark Twain",
    ],
    answer: "William Shakespeare",
  },
];

export default quizData;
