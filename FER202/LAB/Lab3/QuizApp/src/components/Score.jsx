// src/components/Result.js
import React from "react";

const Score = ({ score, total }) => {
  return (
    <div>
      <h1 className="">Congratulation! You have finished your quiz</h1>
      <h2>
        Your Score: {score} / {total}
      </h2>
    </div>
  );
};

export default Score;
