import React from "react";
import "../styles/styles.css";

const Question = ({ question, options, handleAnswer }) => {
  return (
    <div>
      <div className="wrapper bg-white rounded">
        <div className="content">
          <p className="text-muted">Multiple Choice Question</p>
          <p className="text-justify h5 pb-2 font-weight-bold">
            Question {question.id}: {question.question}
          </p>
          {options.map((item, index) => (
            <div key={index}>
              <div className="options py-3">
                <label className="rounded p-2 option" key={index}>
                  {item}
                  <input
                    type="radio"
                    name="radio"
                    onClick={() => handleAnswer(item)}
                  />
                </label>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Question;
