import React from "react";
import { Link } from "react-router-dom";
import newsData from "../data/news";

const Home = () => {
  return (
    <div>
      <h2>Welcome to our news website!</h2>
      <img
        class="rounded w-75 h-50"
        src="https://images.unsplash.com/photo-1682687220989-cbbd30be37e9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1740&q=80"
      ></img>
      <p>
        We are a global non-profit that funds the best and brightest individuals
        around the world dedicated to our mission to use the power of science,
        exploration, education, and storytelling to illuminate and protect the
        wonder of our world.
      </p>
    </div>
  );
};

export default Home;
