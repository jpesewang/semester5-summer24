

function About() {
  document.title = "Vy Newspaper | About Us";
  return (
    <div>
      <h1>About page</h1>
      <br></br>
      <section>
        <div>
          <h1>Hi, I'm Leif</h1>
          <h3>Software Developer</h3>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </p>
        </div>
      </section>
    </div>
  );
}
export default About;
