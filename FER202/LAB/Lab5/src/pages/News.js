import React from "react";
import { useParams } from "react-router-dom";
import newsData from "../data/news";

const NewsArticle = () => {
  const { id } = useParams();
  const article = newsData.find((article) => article.id === parseInt(id, 10));

  if (!article) {
    return <div>This article does not exist</div>;
  }

  return (
    <div>
      <h2>{article.title}</h2>
      <img
        className="rounded w-75 h-50"
        src={article.url}
        alt={article.title}
      />
      <p>{article.content}</p>
    </div>
  );
};

export default NewsArticle;
