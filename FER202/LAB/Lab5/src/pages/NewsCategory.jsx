import React from 'react';
import Card from 'react-bootstrap/Card';
import newLists from '../data/newLists';

export default function NewsCategory() {
  return (
    <div>
      <h1>News Category</h1>
      <div className="row">
        {newLists.map((item, index) => (
          <div className="col-lg-3 col-md-6 mb-4" key={index}>
            <Card style={{ width: '100%' }}>
              <Card.Img variant="top" src={item.images} />
              <Card.Body>
                <Card.Title>{item.title}</Card.Title>
                <Card.Text>{item.description}</Card.Text>
                <Card.Link href="#">Read more</Card.Link>
              </Card.Body>
            </Card>
          </div>
        ))}
      </div>
    </div>
  );
}
