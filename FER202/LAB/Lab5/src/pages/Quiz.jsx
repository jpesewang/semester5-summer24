import React, { useContext } from 'react';
import { QuizContext } from '../context/QuizProvider';// Adjust the import path as necessary

const Quiz = () => {
    const {
      quizData,
      currentQuestionIndex,
      userAnswers,
      score,
      handleAnswerChange,
      nextQuestion,
      checkAnswers
    } = useContext(QuizContext);
  
    const currentQuestion = quizData[currentQuestionIndex];
  
    const handleSubmit = () => {
      if (currentQuestionIndex < quizData.length - 1) {
        nextQuestion();
      } else {
        checkAnswers();
      }
    };
  
    return (
      <div>
        {score === null ? (
          <div>
            <h3>{currentQuestion.question}</h3>
            {currentQuestion.answers.map((answer, idx) => (
              <div key={idx}>
                <input
                  type="radio"
                  name={currentQuestion.question}
                  value={answer}
                  checked={userAnswers[currentQuestion.question] === answer}
                  onChange={() => handleAnswerChange(currentQuestion.question, answer)}
                />
                <label>{answer}</label>
              </div>
            ))}
            <button onClick={handleSubmit}>
              {currentQuestionIndex < quizData.length - 1 ? 'Next Question' : 'Submit'}
            </button>
          </div>
        ) : (
          <div>
            <h3>Your Score: {score} / {quizData.length}</h3>
          </div>
        )}
      </div>
    );
  };
  
  export default Quiz;