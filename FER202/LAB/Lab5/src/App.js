import React from "react";
import {
  BrowserRouter as Router,
} from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import routes from "./routes/routes";
import { QuizProvider } from "./context/QuizProvider";
const App = () => {
  return (
    <Router>
      <QuizProvider>
        <Header />
        {routes}
        <Footer />
      </QuizProvider>
    </Router>
  );
};

export default App;
