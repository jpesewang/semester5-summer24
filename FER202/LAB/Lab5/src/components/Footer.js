import React from "react";
import { Container } from "react-bootstrap";

const Footer = () => {
  return (
    <footer className="bg-light py-3">
      <Container className="d-flex justify-content-center align-items-center h-100">
        <p className="m-0">© 2024 Le Nhat Quang's LAB5. All rights reserved.</p>
      </Container>
    </footer>
  );
};

export default Footer;
