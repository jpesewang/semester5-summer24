import React from "react";
import { Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";


const NavUrl = [
  {
    key: 1,
    title: "About",
    href: "/about"
  },
  {
    key: 2,
    title: "News",
    href: "/news"
  },
  {
    key: 3,
    title: "Quiz",
    href: "/quizzies"
  },
  {
    key: 4,
    title: "Contact",
    href: "/contact"
  }
]
const Header = () => {
  return (
    <header>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">
          Home
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbar-nav" />
        <Navbar.Collapse id="navbar-nav">
          <Nav className="ml-auto">
            {NavUrl.map((item) => (
              <Nav.Link
                as={Link}
                to={`${item.href}`}
                key={item.key}
              >
                {item.title}
              </Nav.Link>
            ))}

          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </header>
  );
};

export default Header;
