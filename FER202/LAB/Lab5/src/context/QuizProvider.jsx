import React, { useState, createContext } from 'react';
import { quizData } from '../data/quizData';
// Create a context
const QuizContext = createContext();
// Context provider component
const QuizProvider = ({ children }) => {
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [userAnswers, setUserAnswers] = useState({});
    const [score, setScore] = useState(null);

    const handleAnswerChange = (question, answer) => {
        setUserAnswers(prevAnswers => ({
            ...prevAnswers,
            [question]: answer
        }));
    };

    const nextQuestion = () => {
        setCurrentQuestionIndex(prevIndex => prevIndex + 1);
    };

    const checkAnswers = () => {
        let newScore = 0;
        quizData.forEach(q => {
            if (userAnswers[q.question] === q.correctAnswer) {
                newScore += 1;
            }
        });
        setScore(newScore);
    };

    return (
        <QuizContext.Provider value={{
            quizData,
            currentQuestionIndex,
            userAnswers,
            score,
            handleAnswerChange,
            nextQuestion,
            checkAnswers
        }}>
            {children}
        </QuizContext.Provider>
    );
};

export { QuizContext, QuizProvider };