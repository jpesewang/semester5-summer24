import React from "react";
import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import NewsArticle from "../pages/News";
import About from "../pages/About";
import Contact from "../pages/Contact";
import NewsCategory from "../pages/NewsCategory";
import NotFound from "../components/NotFound";
import Quiz from "../pages/Quiz";

const routes = (
  <Routes>
    <Route path="/" element={<Home />} />
    <Route path="/article/:id" element={<NewsArticle />} />
    <Route path="/about" element={<About />} />
    <Route path="/contact" element={<Contact />} />
    <Route path="/quizzies" element={<Quiz />} />
    <Route path="/news" element={<NewsCategory />} />
    <Route path="*" element={<NotFound />} />
  </Routes>
);

export default routes;
