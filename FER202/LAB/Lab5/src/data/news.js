const newsData = [
  {
    id: 1,
    title: "Article 1",
    content: "Youngsters are addicted with Anime.",
    url: "https://images.unsplash.com/photo-1687186735445-df877226fae9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80",
  },
  {
    id: 2,
    title: "Article 2",
    content: "Breathtaking view.",
    url: "https://images.unsplash.com/photo-1687220297381-f8fddaa09163?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80",
  },
  {
    id: 3,
    title: "Article 3",
    content: "Let's get on Adventure.",
    url: "https://images.unsplash.com/photo-1687120486460-a12217b24b92?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80",
  },
];

export default newsData;
