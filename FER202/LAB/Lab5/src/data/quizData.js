export const quizData = [
    {
      question: 'What is ReactJS?',
      answers: [
        'A JavaScript library for building user interfaces',
        'A programming language',
        'A database management system'
      ],
      correctAnswer: 'A JavaScript library for building user interfaces'
    },
    {
      question: 'What is JSX?',
      answers: [
        'A programming language',
        'A file format',
        'A syntax extension for JavaScript'
      ],
      correctAnswer: 'A syntax extension for JavaScript'
    },
    {
      question: 'Which company developed ReactJS?',
      answers: [
        'Google',
        'Facebook',
        'Microsoft'
      ],
      correctAnswer: 'Facebook'
    },
    {
      question: 'What is a component in React?',
      answers: [
        'A reusable piece of UI',
        'A CSS style sheet',
        'A JavaScript function'
      ],
      correctAnswer: 'A reusable piece of UI'
    },
    {
      question: 'What is the use of useState in React?',
      answers: [
        'To handle side effects',
        'To manage state in a functional component',
        'To fetch data from an API'
      ],
      correctAnswer: 'To manage state in a functional component'
    },
    {
      question: 'What is the use of useEffect in React?',
      answers: [
        'To handle side effects',
        'To manage state',
        'To create context'
      ],
      correctAnswer: 'To handle side effects'
    },
    {
      question: 'Which hook is used to manage context in React?',
      answers: [
        'useState',
        'useEffect',
        'useContext'
      ],
      correctAnswer: 'useContext'
    },
    {
      question: 'What does the Virtual DOM do in React?',
      answers: [
        'Interacts directly with the real DOM',
        'Acts as an abstraction over the real DOM to optimize updates',
        'Manages the state of components'
      ],
      correctAnswer: 'Acts as an abstraction over the real DOM to optimize updates'
    },
    {
      question: 'What is the purpose of prop drilling in React?',
      answers: [
        'To pass data from parent to child components',
        'To manage global state',
        'To handle events'
      ],
      correctAnswer: 'To pass data from parent to child components'
    },
    {
      question: 'What is the purpose of React Router?',
      answers: [
        'To manage component state',
        'To handle side effects',
        'To manage navigation in a React application'
      ],
      correctAnswer: 'To manage navigation in a React application'
    }
  ];
  