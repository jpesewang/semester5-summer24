import React from "react";
import { Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";


const NavUrl = [
  {
    key: 1,
    title: "Exercise 1",
    href: "/exercise1"
  },
  {
    key: 2,
    title: "Exercise 2",
    href: "/exercise2"
  },
  {
    key: 3,
    title: "Exercise 3",
    href: "/exercise3"
  },
  {
    key: 4,
    title: "Exercise 4",
    href: "/exercise4"
  }
]
const Navigation = () => {
  return (
    <header>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/"  className="mx-4">
          Le Nhat Quang's Exercise - Week 1
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbar-nav" />
        <Navbar.Collapse id="navbar-nav">
          <Nav className="ml-auto">
            {NavUrl.map((item) => (
              <Nav.Link
                as={Link}
                to={`${item.href}`}
                key={item.key}
              >
                {item.title}
              </Nav.Link>
            ))}

          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </header>
  );
};

export default Navigation;
