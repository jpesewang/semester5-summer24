import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css"; // Assuming the styles are added to App.css

const CardExample = () => {
  return (
    <div className="container">
      <div className="card-container">
        <div className="row">
          <div className="col-12">
            <h2>Card Column</h2>
          </div>
        </div>
        <div className="row">
          {/* Card 1 */}
          <div className="col-md-4">
            <div className="card bg-green">
              <img
                src="https://vnn-imgs-f.vgcloud.vn/2019/03/19/18/o-to-moi-3.jpg"
                className="card-img-top"
                alt="Image 1"
              />
              <div className="card-body">
                <p className="card-text">Some text inside the first card</p>
              </div>
            </div>
          </div>
          {/* Card 2 */}
          <div className="col-md-4">
            <div className="card bg-yellow">
              <img
                src="https://vnn-imgs-f.vgcloud.vn/2019/03/19/18/o-to-moi-3.jpg"
                className="card-img-top"
                alt="Image 2"
              />
              <div className="card-body">
                <p className="card-text">Some text inside the second card</p>
              </div>
            </div>
          </div>
          {/* Card 3 */}
          <div className="col-md-4">
            <div className="card bg-red">
              <img
                src="https://vnn-imgs-f.vgcloud.vn/2019/03/19/18/o-to-moi-3.jpg"
                className="card-img-top"
                alt="Image 3"
              />
              <div className="card-body">
                <p className="card-text">Some text inside the third card</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardExample;
