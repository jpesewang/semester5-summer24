import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-social/bootstrap-social.css";
import "./style.css"; // Adjust the path as necessary for your project structure

const App = () => {
  return (
    <div>
      <header>
        <nav className="navbar navbar-expand-sm bg-white fixed-top">
          <div className="container">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Navbar
                </a>
              </li>
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Link
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Dropdown
                </a>
              </li>
            </ul>

            <div className="row">
              <div className="col">
                <div className="input-group">
                  <input
                    type="password"
                    className="form-control"
                    id="inputPassword2"
                    placeholder="Search"
                  />
                  <div className="input-group-append">
                    <button type="submit" className="btn btn-primary">
                      <i className="fa-solid fa-search"></i> Search
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </header>

      <main className="container row-margin">
        <div className="row">
          <div className="col-md-6">
            <div className="card">
              <img
                src="https://4.bp.blogspot.com/-c7uL6mwCuAM/VXGmJspkTyI/AAAAAAAAGtU/cMTrMkuOAI0/s1600/hinh-anh-nen-hd-sieu-ro-net-cuc-dep-cho-may-tinh-img0251.jpg"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  This is a shorter card with supporting text below as a natural
                  lead-in to additional content.
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="card">
              <img
                src="https://upanh123.com/wp-content/uploads/2020/10/hinh-anh-thie-nhien-dep-nhat-the-gioi.jpg"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  This is a shorter card with supporting text below as a natural
                  lead-in to additional content.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <div className="card">
              <img
                src="https://upanh123.com/wp-content/uploads/2019/08/hinh-anh-dep-ve-thien-nhien-3d.jpg"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  This is a shorter card with supporting text below as a natural
                  lead-in to additional content.
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="card">
              <img
                src="https://1.bp.blogspot.com/-bsdv2QpcnCk/UkQUxQNDa7I/AAAAAAAAAkE/W-KXpvYxyHA/s1600/anh-dep-hinh-nen-thien-nhien-20.jpg"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  This is a shorter card with supporting text below as a natural
                  lead-in to additional content.
                </p>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default App;
