import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-social/bootstrap-social.css";
import "./style.css";

const App = () => {
  return (
    <div>
      <header>
        <nav className="navbar navbar-expand-sm bg-white fixed-top">
          <div className="container">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Navbar
                </a>
              </li>
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Link
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Dropdown
                </a>
              </li>
            </ul>
            <div className="input-group">
              <input
                type="text"
                className="form-control"
                placeholder="Search"
              />
              <div className="input-group-append">
                <button className="btn btn-primary" type="button">
                  <i className="fas fa-search"></i> Search
                </button>
              </div>
            </div>
          </div>
        </nav>
      </header>

      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-ride="carousel"
      >
        <div className="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              src="https://4.bp.blogspot.com/-c7uL6mwCuAM/VXGmJspkTyI/AAAAAAAAGtU/cMTrMkuOAI0/s1600/hinh-anh-nen-hd-sieu-ro-net-cuc-dep-cho-may-tinh-img0251.jpg"
              className="d-block w-100"
              alt="Slide 1"
            />
          </div>
          <div className="carousel-item">
            <img
              src="https://upanh123.com/wp-content/uploads/2020/10/hinh-anh-thie-nhien-dep-nhat-the-gioi.jpg"
              className="d-block w-100"
              alt="Slide 2"
            />
          </div>
          <div className="carousel-item">
            <img
              src="https://upanh123.com/wp-content/uploads/2020/10/hinh-anh-thie-nhien-dep-nhat-the-gioi4-1024x576.jpg"
              className="d-block w-100"
              alt="Slide 3"
            />
          </div>
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleIndicators"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleIndicators"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>

      <div className="container row-margin">
        <div className="row">
          <h2>NEW PRODUCT</h2>
          <div>List product description</div>
          <div className="col-md-3">
            <img
              src="https://upanh123.com/wp-content/uploads/2019/08/hinh-anh-dep-ve-thien-nhien-3d.jpg"
              className="img-fluid img-style"
              alt="Image 1"
            />
          </div>
          <div className="col-md-3">
            <img
              src="https://1.bp.blogspot.com/-bsdv2QpcnCk/UkQUxQNDa7I/AAAAAAAAAkE/W-KXpvYxyHA/s1600/anh-dep-hinh-nen-thien-nhien-20.jpg"
              className="img-fluid img-style"
              alt="Image 2"
            />
          </div>
          <div className="col-md-3">
            <img
              src="https://tse2.mm.bing.net/th?id=OIP.9kcfTuy4-b7YqiE7CiFwFwHaEo&pid=Api&P=0&h=180"
              className="img-fluid img-style"
              alt="Image 3"
            />
          </div>
          <div className="col-md-3">
            <img
              src="https://haycafe.vn/wp-content/uploads/2022/03/Hinh-anh-nui-Phu-Si-dep.jpg"
              className="img-fluid img-style"
              alt="Image 4"
            />
          </div>
        </div>
      </div>

      <div className="container row-margin">
        <div className="row">
          <h2>NEW PRODUCT</h2>
          <div>List product description</div>
          <div className="col-md-3">
            <div className="card" style={{ width: "17.687rem" }}>
              <img
                src="https://upanh123.com/wp-content/uploads/2019/08/hinh-anh-dep-ve-thien-nhien-3d.jpg"
                className="card-img-top img-style2"
                alt="Product 1"
              />
              <div className="card-body">
                <div className="container">
                  <div className="row">
                    <p className="card-text">Product</p>
                    <p className="card-text col-6">
                      <s>100.000vnd</s>
                    </p>
                    <p className="card-text col-6 color-style">20.000vnd</p>
                    <div className="align-items-center">
                      <button className="btn btn-primary">
                        <i className="fas fa-cart-shopping logo-style"></i>
                      </button>
                      <button className="btn btn-outline-primary">
                        Xem chi tiết
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="card" style={{ width: "17.687rem" }}>
              <img
                src="https://1.bp.blogspot.com/-bsdv2QpcnCk/UkQUxQNDa7I/AAAAAAAAAkE/W-KXpvYxyHA/s1600/anh-dep-hinh-nen-thien-nhien-20.jpg"
                className="card-img-top img-style2"
                alt="Product 2"
              />
              <div className="card-body">
                <div className="container">
                  <div className="row">
                    <p className="card-text">Product</p>
                    <p className="card-text col-6">
                      <s>100.000vnd</s>
                    </p>
                    <p className="card-text col-6 color-style">20.000vnd</p>
                    <div className="align-items-center">
                      <button className="btn btn-primary">
                        <i className="fas fa-cart-shopping logo-style"></i>
                      </button>
                      <button className="btn btn-outline-primary">
                        Xem chi tiết
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="card" style={{ width: "17.687rem" }}>
              <img
                src="https://tse2.mm.bing.net/th?id=OIP.9kcfTuy4-b7YqiE7CiFwFwHaEo&pid=Api&P=0&h=180"
                className="card-img-top img-style2"
                alt="Product 3"
              />
              <div className="card-body">
                <div className="container">
                  <div className="row">
                    <p className="card-text">Product</p>
                    <p className="card-text col-6">
                      <s>100.000vnd</s>
                    </p>
                    <p className="card-text col-6 color-style">20.000vnd</p>
                    <div className="align-items-center">
                      <button className="btn btn-primary">
                        <i className="fas fa-cart-shopping logo-style"></i>
                      </button>
                      <button className="btn btn-outline-primary">
                        Xem chi tiết
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="card" style={{ width: "17.687rem" }}>
              <img
                src="https://haycafe.vn/wp-content/uploads/2022/03/Hinh-anh-nui-Phu-Si-dep.jpg"
                className="card-img-top img-style2"
                alt="Product 4"
              />
              <div className="card-body">
                <div className="container">
                  <div className="row">
                    <p className="card-text">Product</p>
                    <p className="card-text col-6">
                      <s>100.000vnd</s>
                    </p>
                    <p className="card-text col-6 color-style">20.000vnd</p>
                    <div className="align-items-center">
                      <button className="btn btn-primary">
                        <i className="fas fa-cart-shopping logo-style"></i>
                      </button>
                      <button className="btn btn-outline-primary">
                        Xem chi tiết
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      <script src="js/scripts.js"></script>
    </div>
  );
};

export default App;
