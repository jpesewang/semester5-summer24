import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

const FlightBookingForm = () => {
  const [form, setForm] = useState({
    fullName: "",
    address: "",
    from: "Đà Nẵng",
    to: "Đà Nẵng",
    oneWay: false,
    roundTrip: false,
  });

  const handleChange = (e) => {
    const { name, value, checked, type } = e.target;
    setForm({
      ...form,
      [name]: type === "checkbox" ? checked : value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Form submitted:", form);
  };

  return (
    <div className="container form-container">
      <div className="card-container">
        <div
          className="alert alert-warning alert-dismissible fade show"
          role="alert"
        >
          <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="fullName">Họ và tên</label>
            <input
              type="text"
              className="form-control"
              id="fullName"
              name="fullName"
              value={form.fullName}
              onChange={handleChange}
              placeholder="Họ tên"
            />
          </div>
          <div className="form-group">
            <label htmlFor="address">Địa chỉ</label>
            <input
              type="text"
              className="form-control"
              id="address"
              name="address"
              value={form.address}
              onChange={handleChange}
              placeholder=""
            />
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="from">Đi từ</label>
              <select
                className="form-control"
                id="from"
                name="from"
                value={form.from}
                onChange={handleChange}
              >
                <option>Đà Nẵng</option>
                <option>Tp.HCM</option>
                <option>Tp.HN</option>
              </select>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="to">Đến</label>
              <select
                className="form-control"
                id="to"
                name="to"
                value={form.to}
                onChange={handleChange}
              >
                <option>Đà Nẵng</option>
                <option>Tp.HCM</option>
                <option>Tp.HN</option>
              </select>
            </div>
          </div>
          <div className="form-group">
            <label>Chiều khứ hồi</label>
            <div className="form-check">
              <input
                type="checkbox"
                className="form-check-input"
                id="oneWay"
                name="oneWay"
                checked={form.oneWay}
                onChange={handleChange}
              />
              <label className="form-check-label" htmlFor="oneWay">
                Một chiều
              </label>
            </div>
            <div className="form-check">
              <input
                type="checkbox"
                className="form-check-input"
                id="roundTrip"
                name="roundTrip"
                checked={form.roundTrip}
                onChange={handleChange}
              />
              <label className="form-check-label" htmlFor="roundTrip">
                Khứ hồi
              </label>
            </div>
          </div>
          <button type="submit" className="btn btn-primary btn-book">
            Đặt vé
          </button>
        </form>
      </div>
    </div>
  );
};

export default FlightBookingForm;
