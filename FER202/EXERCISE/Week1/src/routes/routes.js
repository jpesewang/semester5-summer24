import React from "react";
import { Route, Routes } from "react-router-dom";
import NotFound from "../components/NotFound";
import Exercise1 from "../pages/Exercise 1/Exercise1";
import Exercise2 from "../pages/Exercise 2/Exercise2";
import Exercise3 from "../pages/Exercise 3/Exercise3";
import Exercise4 from "../pages/Exercise 4/Exercise4";

const routes = (
  <Routes>
    
    <Route path="/exercise1" element={<Exercise1 />} />
    <Route path="/exercise2" element={<Exercise2 />} />
    <Route path="/exercise3" element={<Exercise3 />} />
    <Route path="/exercise4" element={<Exercise4 />} />
    <Route path="*" element={<NotFound />} />
  </Routes>
);

export default routes;
