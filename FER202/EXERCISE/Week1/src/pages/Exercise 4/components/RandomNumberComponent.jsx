import React, { useState } from 'react';

const RandomNumberComponent = () => {
  const [result, setResult] = useState(null);
  const [error, setError] = useState(null);

  const generateRandomNumber = () => {
    return new Promise((resolve, reject) => {
      const randomNumber = Math.floor(Math.random() * 10) + 1;
      if (randomNumber > 5) {
        resolve(randomNumber);
      } else {
        reject('Error');
      }
    });
  };

  const handleClick = () => {
    generateRandomNumber()
      .then(number => {
        setResult(number);
        setError(null);
      })
      .catch(error => {
        setResult(null);
        setError(error);
      });
  };

  return (
    <div>
      <button onClick={handleClick}>Generate Random Number</button>
      {result && <div>Random Number: {result}</div>}
      {error && <div style={{ color: 'red' }}>{error}</div>}
    </div>
  );
};

export default RandomNumberComponent;
