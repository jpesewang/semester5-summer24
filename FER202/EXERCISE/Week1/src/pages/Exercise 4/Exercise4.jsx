// Exercise 1_Setting up Node.js and NPM
import React from 'react'
import "./Exercise4.css"
import logo from "./assets/React-icon.svg.png"
import Navbar from './Navbar/Navbar'
import CourseList from './components/CourseList'
import PeopleInfo from './components/PeopleInfo'

export default function Exercise4() {

    return (
        <div className='mx-5'>
            <h1>1. Design this website as image below</h1>
            <div className="hello-react">
                Hello <span className="react-text">React</span>
            </div>

            <br></br>
            <h1>2. Design this website as image below</h1>
            <div style={{ textAlign: "center" }}>
                {" "}
                {/* Center align the content */}
                <img
                    src={logo}
                    alt="React Logo"
                    style={{ width: "200px", marginTop: "50px" }}
                />{" "}
                {/* Image with styles */}
                <p style={{ fontStyle: "italic" }}>
                    This is the React logo! <br />
                    (I don't know why it is here either)
                </p>
                <p>The library for web and native user interfaces</p>
            </div>

            <br></br>
            <h1>3. Create a navbar as image below with JSX</h1>
            <Navbar />

            <br></br>
            <h1>4. Display this text</h1>
            <h2 style={{ fontWeight: "bold", color: "blue" }}>This is JSX</h2>

            <br></br>
            <h1>5. Display list of course</h1>
            <h2>My Courses</h2>
            <CourseList />

            <br></br>
            <h1>Using ES6 and JSX</h1>
            <h2>1. PeopleInfo </h2>
            <PeopleInfo />
            <h2>1. PeopleInfo </h2>
        </div>
    )
}
