import React from "react";
import {
  BrowserRouter as Router,
} from "react-router-dom";
import routes from "./routes/routes";
import Navigation from "./components/Navigation";
const App = () => {
  return (
    <Router>
      <Navigation />
        {routes}
    </Router>
  );
};

export default App;
