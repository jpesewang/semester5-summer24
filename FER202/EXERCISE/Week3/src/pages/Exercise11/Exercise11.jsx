import React from "react";
import TodoList from "./components/Todolist";
import Calculator from "./components/Calculator";
import SearchFilter from "./components/SearchFilter";

export default function Exercise11() {
  return (
    <div>
      Exercise11
      <TodoList />
      <Calculator />
      <SearchFilter />
    </div>
  );
}
