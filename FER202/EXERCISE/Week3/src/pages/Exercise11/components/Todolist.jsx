import React, { useState } from "react";

const TodoList = () => {
  const [items, setItems] = useState([]);
  const [newItem, setNewItem] = useState("");

  // Function to add an item to the list
  const addItem = () => {
    if (newItem.trim()) {
      setItems((prevItems) => [...prevItems, newItem]);
      setNewItem("");
    }
  };

  // Function to delete an item from the list
  const deleteItem = (index) => {
    setItems((prevItems) => prevItems.filter((item, i) => i !== index));
  };

  return (
    <div>
      <input
        type="text"
        value={newItem}
        onChange={(e) => setNewItem(e.target.value)}
        placeholder="Enter an item"
      />
      <button onClick={addItem}>Add List</button>
      <h3>List of Items</h3>
      <ul>
        {items.map((item, index) => (
          <li key={index}>
            {item}
            <button onClick={() => deleteItem(index)}>Delete</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TodoList;
