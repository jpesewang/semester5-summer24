import React, { useState } from "react";

export default function SearchFilter() {
  const [query, setQuery] = useState("");
  const [items] = useState([
    "Apple",
    "Banana",
    "Cherry",
    "Date",
    "Elderberry",
    "Fig",
    "Grape",
    "Honeydew",
  ]);

  const handleSearchChange = (event) => {
    setQuery(event.target.value);
  };

  const filteredItems = items.filter((item) =>
    item.toLowerCase().includes(query.toLowerCase())
  );

  return (
    <div style={{ padding: "20px" }}>
      <h1>Search Filter</h1>
      <input
        type="text"
        placeholder="Search items..."
        value={query}
        onChange={handleSearchChange}
        style={{ marginBottom: "20px", padding: "10px", width: "100%" }}
      />
      <ul>
        {filteredItems.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
    </div>
  );
}
