import React, { useState } from "react";

export default function DragAndDropList() {
  const [items, setItems] = useState([
    "Item 1",
    "Item 2",
    "Item 3",
    "Item 4",
    "Item 5",
  ]);
  const [draggingItem, setDraggingItem] = useState(null);

  const handleDragStart = (index) => {
    setDraggingItem(index);
  };

  const handleDragEnter = (index) => {
    const newList = [...items];
    const item = newList.splice(draggingItem, 1)[0];
    newList.splice(index, 0, item);

    setDraggingItem(index);
    setItems(newList);
  };

  const handleDragEnd = () => {
    setDraggingItem(null);
  };

  return (
    <div style={{ padding: "20px" }}>
      <h1>Drag and Drop List</h1>
      <ul style={{ listStyleType: "none", padding: 0 }}>
        {items.map((item, index) => (
          <li
            key={index}
            draggable
            onDragStart={() => handleDragStart(index)}
            onDragEnter={() => handleDragEnter(index)}
            onDragEnd={handleDragEnd}
            style={{
              padding: "10px",
              margin: "5px 0",
              backgroundColor: "lightgray",
              cursor: "move",
              borderRadius: "5px",
            }}
          >
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
}
