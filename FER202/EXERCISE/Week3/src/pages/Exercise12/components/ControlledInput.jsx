import React from "react";
import { useState } from "react";

export default function ControlledInput() {
  const [input, setInput] = useState("");
  return (
    <div>
      ControlledInput
      <input value={input} onChange={(e) => setInput(e.target.value)} />
      Input text: {input}
    </div>
  );
}
