import React, { useState } from "react";

export default function ColorSwitcher() {
  const [color, setColor] = useState("");

  const handleColorChange = (event) => {
    setColor(event.target.value);
  };

  return (
    <div style={{ padding: "20px" }}>
      <h1>Color Switcher</h1>
      <select onChange={handleColorChange} value={color}>
        <option value="">Select a color</option>
        <option value="red">Red</option>
        <option value="blue">Blue</option>
        <option value="green">Green</option>
        <option value="yellow">Yellow</option>
      </select>
      <div
        style={{
          marginTop: "20px",
          width: "100%",
          height: "200px",
          backgroundColor: color,
          border: "1px solid #000",
        }}
      ></div>
    </div>
  );
}
