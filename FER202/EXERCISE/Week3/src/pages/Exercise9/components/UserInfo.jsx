import React from "react";

const UserInfo = () => {
  return (
    <div style={{ textAlign: "center", margin: "20px" }}>
      <h1>Quang Le</h1>
      <p>
        Hello, I'm Hoai Nguyen from FPT University in Da Nang. Passionate about
        technology and education!
      </p>
    </div>
  );
};

export default UserInfo;
