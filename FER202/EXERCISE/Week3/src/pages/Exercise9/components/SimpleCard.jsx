import React from "react";

const BusinessCard = () => {
  return (
    <div
      style={{
        border: "1px solid #000",
        padding: "20px",
        maxWidth: "500px",
        margin: "auto",
      }}
    >
      <div
        style={{ display: "flex", alignItems: "center", marginBottom: "15px" }}
      >
        <img
          src="/path-to-fpt-education-logo.png" // Replace with the path to your image
          alt="FPT Education Logo"
          style={{ marginRight: "15px" }}
        />
        <h1 style={{ margin: 0 }}>FPT UNIVERSITY</h1>
      </div>
      <div>
        <strong>Hoai Nguyen - FPT DaNang</strong>
        <p>Mobile: 098227763</p>
      </div>
    </div>
  );
};

export default BusinessCard;
