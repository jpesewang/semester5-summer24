import React from "react";
import "./Header.css"; // Importing CSS file for Header

const Header = () => (
  <header
    style={{ background: "orange", padding: "10px 0", textAlign: "center" }}
  >
    {/* Logo and title would go here */}
    <img
      src="/path-to-your-logo.png"
      alt="FPT University Logo"
      style={{ height: "100px" }}
    />
  </header>
);

const Navigation = () => (
  <nav
    style={{
      background: "black",
      color: "white",
      textAlign: "center",
      padding: "5px 0",
    }}
  >
    <a href="/" style={{ color: "white", margin: "0 10px" }}>
      Home
    </a>
    <a href="/about" style={{ color: "white", margin: "0 10px" }}>
      About
    </a>
    <a href="/contact" style={{ color: "white", margin: "0 10px" }}>
      Contact
    </a>
  </nav>
);

const AboutSection = () => (
  <section style={{ margin: "20px 0", textAlign: "center" }}>
    <h2>About</h2>
    <p>This is the about section of the website.</p>
  </section>
);

const ContactSection = () => (
  <section style={{ margin: "20px 0", textAlign: "center" }}>
    <h2>Contact</h2>
    <p>For any inquiries, please contact us at example@example.com.</p>
  </section>
);

export default function SimpleWebsite() {
  return (
    <>
      <Header />
      <Navigation />
      <AboutSection />
      <ContactSection />
      {/* <Footer /> */}
    </>
  );
}
