import React, { useState } from "react";

const CounterApp = () => {
  // Initialize the count state variable to 0
  const [count, setCount] = useState(0);

  // Function to increment the count
  const handleIncrement = () => {
    setCount(count + 1);
  };

  // Function to decrement the count
  const handleDecrement = () => {
    setCount(count - 1);
  };

  return (
    <div style={{ textAlign: "center" }}>
      <h2>Counter: {count}</h2>
      <button onClick={handleIncrement}>Increment</button>
      <button onClick={handleDecrement}>Decrement</button>
    </div>
  );
};

export default CounterApp;
